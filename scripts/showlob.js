function createWolfRow(name, age, description, image, data, id) {
    let wolvesRow = document.createElement("div");
    wolvesRow.classList.add("cardLobos");

    // Link para redirecionar para a página de detalhes do lobo
    let linkElement = document.createElement('a');

    linkElement.addEventListener("click", () => {
        localStorage.setItem("selectedWolf", JSON.stringify(data));
        window.location.href = "/showlobinho.html";
    });
    // Imagem do lobo
    let wolfImg = document.createElement("img");
    wolfImg.src = image;
    let wolfImgDiv = document.createElement("div");
    wolfImgDiv.append(wolfImg);
    wolfImgDiv.classList.add("imgDivLobo");
    wolfImgDiv.id = "sombraWolfImgRevese";
    wolvesRow.append(wolfImgDiv);;
    // Botão de adoção
    let wolfAdop = document.createElement("div")
    wolfAdop.classList.add("divBtn")

    let adopBtn = document.createElement("button");
    let adopText = document.createElement("h1");
    adopBtn.classList.add("btnAdop");
    if (data.adotado == true) {
        adopText.innerText = "Adotado";
    }
    else {
        adopText.innerText = "Adotar";
        adopBtn.addEventListener('click', () => {
            window.location.href = "../adicionar-lobinho.html";
        })
    }
    adopText.classList.add("htext");
    adopBtn.append(adopText);
    wolfAdop.append(adopBtn);
    // Informações do lobo (nome, idade, descrição)
    let wolfText = document.createElement("div");
    wolfText.classList.add("loboTexto");
    let wolfName = document.createElement("h2");
    wolfName.classList.add("btnnome")
    wolfName.innerText = name;
    let wolfAge = document.createElement("span");
    wolfAge.innerText = ("Idade: " + age);
    let wolfDscription = document.createElement("p");
    wolfDscription.innerText = description;
    wolfText.append(wolfName);
    wolfText.append(wolfAge);
    wolfText.append(wolfDscription);
    wolvesRow.append(wolfText);
    wolvesRow.append(adopBtn)
    
    wolfName.addEventListener("click", () => {
        window.location.href = `../show-lobinho.html?id=${id}`;
    });
    return wolvesRow;
}
function setReverseWolfRow(name, age, description, image, data, id) {
    wolvesRow = createWolfRow(name, age, description, image, data, id);
    wolvesRow.id = "cardLobosReverso"
    let wolvesSection = document.getElementById("loboShow");
    wolvesSection.append(wolvesRow);
}



document.addEventListener("DOMContentLoaded", async () => {
    const response = await fetch('../api/lobinhos.json');
    const data = await response.json();
    const urlParams = new URLSearchParams(window.location.search)
    const lobID = parseInt(urlParams.get("id"))
    setReverseWolfRow(data[lobID].nome, data[lobID].idade, data[lobID].descricao, data[lobID].imagem, data[lobID], data[lobID].id)  ;
});

async function deleteLobinho(id) {

    fetch(`../api/lobinhos/${id}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.json();
    })
    .then(data => {
        console.log('Element deleted successfully:', data);
    })
    .catch(error => {
        console.error('Error:', error);
    });

}


const btnExclui = document.getElementById("btnExcluir")
btnExclui.addEventListener("click", () => {
    const urlParams = new URLSearchParams(window.location.search)
    const lobID = parseInt(urlParams.get("id"))
    deleteLobinho(lobID)
    });
