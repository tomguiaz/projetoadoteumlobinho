
var list = [];

 


async function fetchData(callback){
    let response = await fetch('./api/lobinhos.json');
    let data = await response.json();

    data.forEach(object => {
        list.push(object);
    })

    // showFirstItem(list);
    homepageFillLobos();
    // showLobos();
}

function showFirstItem(list){
    console.log(list[0]);
}

function showLobos(){
    list.forEach((obj)=>console.log(obj.nome));
}
function homepageFillLobos(){

    let card1 = document.querySelector('#card-1');
    let card2 = document.querySelector("#card-2");

    let nome_lobo1 = card1.querySelector(".lobo-nome");
    let nome_lobo2 = card2.querySelector(".lobo-nome");

    nome_lobo1.innerText = list[0].nome;
    nome_lobo2.innerText = list[1].nome;

    let idade_lobo1 = card1.querySelector(".lobo-idade");
    let idade_lobo2 = card2.querySelector(".lobo-idade");

    idade_lobo1.innerText = list[0].idade;
    idade_lobo2.innerText = list[1].idade;

    let descricao_lobo1 = card1.querySelector(".lobo-descricao");
    let descricao_lobo2 = card2.querySelector(".lobo-descricao");

    descricao_lobo1.innerText = list[0].descricao;
    descricao_lobo2.innerText = list[0].descricao;

    let img_lobo1 = card1.querySelector(".lobo-img");
    let img_lobo2 = card2.querySelector(".lobo-img");

    img_lobo1.src=list[0].imagem;
    img_lobo2.src=list[1].imagem;
}
fetchData();

