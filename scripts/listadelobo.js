let generatedNumbers = new Set();
// Gerar um número aleatório único
function generateRandom() {
    let randomNumber;
    do {
        randomNumber = Math.floor(Math.random() * 1000) + 1;
    } while (generatedNumbers.has(randomNumber));
    generatedNumbers.add(randomNumber);
    return randomNumber;
}
// Criar uma card de lobo 
function createWolfRow(name, age, description, image, data, id) {
    let wolvesRow = document.createElement("div");
    wolvesRow.classList.add("cardLobos");

    // Link para redirecionar para a página de detalhes do lobo
    let linkElement = document.createElement('a');

    linkElement.addEventListener("click", () => {
        localStorage.setItem("selectedWolf", JSON.stringify(data));
        window.location.href = "/show-lobinho.html";
    });
    // Imagem do lobo
    let wolfImg = document.createElement("img");
    wolfImg.src = image;
    let wolfImgDiv = document.createElement("div");
    wolfImgDiv.append(wolfImg);
    wolfImgDiv.classList.add("imgDivLobo");
    wolfImgDiv.id = "sombraWolfImgRevese";
    wolvesRow.append(wolfImgDiv);;
    // Botão de adoção
    let wolfAdop = document.createElement("div")
    wolfAdop.classList.add("divBtn")

    let adopBtn = document.createElement("button");
    let adopText = document.createElement("h1");
    adopBtn.classList.add("btnAdop");
    if (data.adotado == true) {
        adopText.innerText = "Adotado";
    }
    else {
        adopText.innerText = "Adotar";
        adopBtn.addEventListener('click', () => {
            window.location.href = "../adotar-lobinho.html";
        })
    }
    adopText.classList.add("htext");
    adopBtn.append(adopText);
    wolfAdop.append(adopBtn);
    // Informações do lobo (nome, idade, descrição)
    let wolfText = document.createElement("div");
    wolfText.classList.add("loboTexto");
    let wolfName = document.createElement("h2");
    wolfName.classList.add("btnnome")
    wolfName.innerText = name;
    let wolfAge = document.createElement("span");
    wolfAge.innerText = ("Idade: " + age);
    let wolfDscription = document.createElement("p");
    wolfDscription.innerText = description;
    wolfText.append(wolfName);
    wolfText.append(wolfAge);
    wolfText.append(wolfDscription);
    wolvesRow.append(wolfText);
    wolvesRow.append(adopBtn)
    
    wolfName.addEventListener("click", () => {
        window.location.href = `../show-lobinho.html?id=${id}`;
    });
    return wolvesRow;
}
// Muda a tag de adoção
function adopted(event) {
    let button = event.target;
    button.innerText = "Adotado";
    button.classList.add("clicked");
    button.removeEventListener("click", adopted);
};


//  Adiciona lista de lobos na seção
function setWolfRow(name, age, description, image, data, id) {
    wolvesRow = createWolfRow(name, age, description, image, data, id);
    let wolvesSection = document.getElementById("lobos-section");
    wolvesSection.append(wolvesRow);
}
function setReverseWolfRow(name, age, description, image, data, id) {
    wolvesRow = createWolfRow(name, age, description, image, data, id);
    wolvesRow.id = "cardLobosReverso"
    let wolvesSection = document.getElementById("lobos-section");
    wolvesSection.append(wolvesRow);
}


// Adicona informações especificas de cada lobo na lista
async function getWolfCard(pageNumber) {
    console.log(`getWolfCard called with pageNumber: ${pageNumber}`); 
    try {
        const response = await fetch('../api/lobinhos.json');
        const data = await response.json();

        const itemsPerPage = 4;
        const startIndex = (pageNumber - 1) * itemsPerPage;
        const endIndex = Math.min(startIndex + itemsPerPage, data.length);

        const wolfList = document.getElementById("lobos-section");
        wolfList.innerHTML = "";

        for (let i = startIndex; i < endIndex; i++) {
            if (i % 2 !== 0) {
                setReverseWolfRow(data[i].nome, data[i].idade, data[i].descricao, data[i].imagem, data[i], data[i].id - 1);
            } else {
                setWolfRow(data[i].nome, data[i].idade, data[i].descricao, data[i].imagem, data[i], data[i].id - 1 );
            }
        }
        // Botões de paginação
        btnPaginacao(pageNumber, Math.ceil(data.length / itemsPerPage));
    } catch (err) {
        console.error(err);
    }
    console.log('getWolfCard finished executing');
}



document.addEventListener("DOMContentLoaded", () => {
    getWolfCard(1);
});

//Adicionando funcionalidade aos botões de paginação
function btnPaginacao(currentPage, totalPages) {
    const paginationContainer = document.getElementById("pagination");
    paginationContainer.innerHTML = "";

    const previousButton = document.createElement("button");
    previousButton.textContent = "Anterior";
    previousButton.disabled = currentPage === 1;
    previousButton.addEventListener("click", () => {
        getWolfCard(currentPage - 1);
    });

    const nextButton = document.createElement("button");
    nextButton.textContent = "Próximo";
    nextButton.disabled = currentPage === totalPages;
    nextButton.addEventListener("click", () => {
        getWolfCard(currentPage + 1);
    });

    paginationContainer.appendChild(previousButton);
    paginationContainer.appendChild(nextButton);
}


// BARRA DE PESQUISA  E CHECKBOX 
function search() {
    let secao = document.querySelector("#lobos-section");
    secao.innerHTML = "";
    let input = document.querySelector(".barra_pesquisa").value.trim(); 
    let showAdotados = document.querySelector(".checkbox").checked;

    const fetchConfig = {
        method: "GET",
    };

    fetch("../api/lobinhos.json", fetchConfig)
        .then((response) => response.json())
        .then((response) => {
            response.forEach((wolf) => {
                const inputLower = input.toLowerCase();
                const nomeLower = wolf.nome.toLowerCase();

                if (
                    (isNaN(input) && nomeLower.includes(inputLower)) || // Se não for um número, pesquisar por nome
                    (wolf.id === parseInt(input) || nomeLower.includes(inputLower)) // Se for um número, pesquisar por ID ou nome
                ) {
                    // Filtrar Adotados
                    if (!showAdotados || (showAdotados && wolf.adotado)) {
                        setWolfRow(wolf.nome, wolf.idade, wolf.descricao, wolf.imagem, wolf);
                    }
                }
            });
        })
        .catch((error) => {
            console.log(error);
        });

    document.querySelector(".barra_pesquisa").value = "";
}
// Botão de Pesquisar 
let searchBtn = document.querySelector(".searchlobos");
searchBtn.addEventListener("click", () => {
    search();
});
// Checkbox de Adotados 
let checkbox = document.querySelector(".checkbox");
checkbox.addEventListener("change", () => {
    search();
});

